#include <stdio.h>
#include <limits.h>
int main (){
    printf("Tipo \t\t Tam \t Min \t\t Max\n");
    //Enteros
    printf("char \t\t %lu \t %d \t\t %d\n",sizeof(char), CHAR_MIN, CHAR_MAX);
    printf("uchar \t\t %lu \t %d \t\t%d\n",sizeof(unsigned char), 0, UCHAR_MAX);
    printf("short \t\t %lu \t %d \t %d\n",sizeof(short), SHRT_MIN, SHRT_MAX);
    printf("ushort \t\t %lu \t %d \t\t %d\n",sizeof(unsigned short), 0, USHRT_MAX);
    printf("int \t\t %lu \t %d \t %d\n",sizeof(int), INT_MIN, INT_MAX);
    printf("uint \t\t %lu \t %d \t\t %d\n",sizeof(unsigned int), INT_MIN, INT_MAX);
    printf("long \t\t %lu \t %d \t %d\n",sizeof(long), LONG_MIN, LONG_MAX);
    printf("ulong \t\t %lu \t %ld \t %ld\n",sizeof(unsigned long),0 , ULONG_MAX);
    printf("long long \t %lu \t %lld \t %lld \n",sizeof(long long), LLONG_MIN, LLONG_MAX);
    printf("ulong long \t %lu \t %ld \t %ld\n",sizeof(unsigned long long),0 , ULONG_LONG_MAX);
    //Puntos Flotantes
    printf("float \t\t %lu \t %e \t %e\n",sizeof(float),__FLT_MIN__, __FLT_MAX__); //%e expresa los nros con notacion cientifica
    printf("double \t\t %lu \t %e \t %e\n",sizeof(double),__DBL_MIN__, __DBL_MAX__);
    printf("long double \t %lu \t %e \t %e\n",sizeof(long double),__LDBL_MIN__,__LDBL_MAX__);
    
}